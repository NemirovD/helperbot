LIBFILES := $(wildcard *.rb) $(wildcard libs/*.rb)

all:

server_update: $(LIBFILES)
	for f in $(LIBFILES); do scp $$f nemirovd.com:programs/HelperBot/$$f; done;

run:
	ruby helperbot.rb
# for file in $(LIBFILES); do  sftp nemirovd.com:~/programs/HelperBot/$$f <<< $'put $${f#*/}'; done;
