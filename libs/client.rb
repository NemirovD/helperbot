require 'yaml'
require 'mumble-ruby'
require_relative 'utils'

class Bot
  @client = nil
  @config = nil
  @plugins= nil

  def process_message(message)
    @plugins.each do |plugin|
      if plugin.is_command?(message)
        plugin.run(@client, message)
      end
    end
  end

  def load_config(filename=ENV["HOME"]+"/.config/helperbot/config.yml")
    if File.exists?(filename)
      @config = YAML.load_file(filename)
    else
      @config = nil
    end
  end

  def instantiate_plugins
    @plugins= []
    old_object_space = ObjectSpace.each_object(Class).to_a
    require_relative 'plugins'
    new_object_space = ObjectSpace.each_object(Class).to_a
    (new_object_space - old_object_space).each do |klass|
      if klass != Plugin
        @plugins << klass.new
      end
    end
  end

  def prepare
    load_config
    instantiate_plugins
    @client = Mumble::Client.new(@config['server_info']['host']) do |conf|
      conf.port     = @config['server_info']['port']
      conf.password = @config['server_info']['pass']
      conf.username = @config['bot_info']['name']
    end

    @client.on_connected do |connected|
      @client.join_channel(@config['bot_info']['default_channel'])
    end

    @client.on_text_message do |message|
      process_message(message)
    end
  end

  def start
    puts "Preparing bot..."
    prepare
    puts "Bot prepared."
    puts "Connecting. One moment..."
    @client.connect
    sleep 1

    puts "Now waiting for private or channel messages..."
    puts "Use CTRL-C to end the program."
    while true
      @plugins.each do |plugin|
        if plugin.is_passive?
          plugin.run(@client)
        end
      end

      sleep 30
    end
    @client.disconnect
  end
end

if __FILE__ == $0
  bot = Bot.new
  bot.start
end
