require_relative 'utils'

class Plugin
  def initialize(command='', passive=false)
    @command = command
    @passive = passive
  end

  def is_command?(message)
    if !is_passive?
      return message.message.downcase.start_with?(@command)
    end
    return false
  end

  def is_passive?
    return @passive
  end

  def run(client, message)
  end
end

class FlipPlugin < Plugin
  def initialize
    super('!flip')
  end

  def run(client, message)
    channel = client.users[message.actor].current_channel
    username= client.users[message.actor].name.capitalize
    outstring = "#{username} flipped "
    if rand(0..1).to_i == 1
        outstring += "heads!"
    else
        outstring += "tails!"
    end
    channel.send_text(outstring)
  end
end

class DicePlugin < Plugin
  def initialize
    super('!roll')
  end

  def run(client, message)
  dice_info = /^!roll ([\d]*)d([\d]+)/.match(message.message)
    if dice_info
      channel = client.users[message.actor].current_channel
      username= client.users[message.actor].name.capitalize
      outstring = "#{username} rolled " + message.message.split(' ')[1] + ", Result(s): "

      (1..dice_info[1].to_i).each do |i|
        outstring += rand(1..dice_info[2].to_i).to_s + ", "
      end

      channel.send_text(outstring)
    end
  end
end

class FapPlugin < Plugin
  def initialize
    super('!fap')
  end

  def run(client, message)
    fapLink = fetch('http://nhentai.net/random/')
    client.users[message.actor].send_text("<a href='#{fapLink.to_s}'>Have Fun! #{fapLink.to_s}</a>")
  end
end


class RemindMePlugin < Plugin
  def initialize
    super('!remind')
  end

  def run(client, message)
    client.users[message.actor].send_text("Command Not Implemented Yet")
  end
end

class ListRemindersPlugin < Plugin
  def initialize
    super('!reminders')
  end

  def run(client, message)
    client.users[message.actor].send_text("Command Not Implemented Yet")
  end
end

class SendReminderPlugin < Plugin
  def initialize
    super(nil, true)
  end

  def run(client)
    client.users.each do |key, user|
      reminders = get_reminders(user.name)
      if reminders
        reminders.each do |reminder|
          send_reminder(reminder)
          delete_reminder(reminder)
        end
      end
    end
    return nil
  end
end
